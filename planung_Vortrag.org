
* Quick'n'Dirty
  Die traditionelle Landeskunde muss vom faktenorientierten Vorgehen zu einer
  auf 'Kulturellen Deutungsmustern' basierenden Wissenschaft übergehen.
* 1. Von der 'Landeskunde' zur Kulturwissenschaft
  Durch Globalisierung und moderne Medien ist die Begegnung mit fremden Welten
  nicht mehr auf den Fremdsprachenunterricht beschränkt, sondern passiert
  ununterbrochen. Dadurch bekommt der 'landeskundliche' Aspekt des
  Fremdsprachenlernens völlig neue Bedeutung und so wird die Landeskunde sich
  wahrscheinlich zu einer eigenen wissenschaftlichen Disziplin entwickeln und
  das Kulturstudium in immer mehr Universitäten auftauchen.
  Altmayer warnt aber vor einer 'institutionalisierten Kulturwissenschaft' die
  jeglichen Praxisbezug verliert. Praxis und Theorie müssen seiner Meinung nach
  verzahnte Teilbereiche darstellen.
* 2. Zur Kritik des 'interkulturellen' Paradigmas in der Landeskunde
  Altmayer sieht drei Probleme:
  1) Gleiche Begriffe sind nicht voneinander abgegrenzt und unterschiedliche
     Begriffe meinen häufig fast gleiche Dinge.
  2) Es wird immer vom Zusammentreffen zweier Kulturen ausgegangen. Hierbei
     seien Kulturen klar abgrenzbare innerhalb weitgehend homogene Gruppen mit
     bestimmten Verhaltens- und Wahrnehmensmustern. Diese Definition ist sowohl
     utopisch als auch Vorurteilsfördernd und unterstützt stereotypisches
     Denken, statt es zu hinterfragen.
  3) Die 'interkulturelle' Landeskunde basiert auf der Wahrnehmung einer
     Zielkultur auf Basis der eigenen Kultur. Lernenden wird also primär eine
     'eigene' Kultur zugeordnet und sie werden im Grunde für ihre Kultur
     'haftbar gemacht' obwohl jeder Lernende einen individuellen Hintergrund
     hat und 'seine Kultur' nur einen Teil seiner Identität ausmacht.
  4) Die Vertreter der 'interkulturellen Landeskunde' gehen davon aus, dass die
     Kommunikation über kulturelle Grenzen hinweg schwieriger ist als in der
     'eigenen' Kultur. Dabei wird davon ausgegangen, dass der Lernende
     monokulturell sozialisiert wurde und deshalb völlig selbstverständlich
     seine eigenen Denkmuster zum Verständnis des anderen anwendet. Eine solche
     monokulturelle Sozialisierung ist aber in der heutigen globalisierten Welt
     unwahrscheinlich bis unmöglich. Zudem zeigen neuere Studien, dass die
     Begegnung mit dem Fremden keineswegs zu sofortiger Ablehnung führt sondern
     die Existenz unterschiedlicher Verhaltensmuster von vornherein mitgedacht
     ist.
* 3. 'Kulturelle Deutungsmuster'
* 4. 'Kulturelles Lernen' und landeskundliche Themenplanung
* 5. Ausblick: Zur Notwendigkeit kulturwissenschaftlicher Forschung
